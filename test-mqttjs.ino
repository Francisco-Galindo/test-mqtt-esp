#include "EspMQTTClient.h"

#define SWITCH 14
#define TIME 500

bool encendido = false;

EspMQTTClient client(
	"SSID",
	"pass",
	"192.168.*****"
);

void setup() {
	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(SWITCH, INPUT);
	Serial.begin(115200);
	digitalWrite(LED_BUILTIN, encendido == LOW);
}


void onConnectionEstablished() {
	Serial.println("Conectado");
	client.publish("led/estado_resp", String(encendido));
	client.subscribe("led/estado_query", [] (const String &payload)  {
		client.publish("led/estado_resp", String(encendido));
	});
	client.subscribe("led/encender", [] (const String &payload)  {
		if (payload.equals("off")) {
			/* digitalWrite(LED_BUILTIN, HIGH); */
			encendido = false;
		} else {
			/* digitalWrite(LED_BUILTIN, LOW); */
			encendido = true;
		}
		digitalWrite(LED_BUILTIN, encendido == LOW);
		client.publish("led/estado_resp", String(encendido));
	});
}

void loop() {
	client.loop();
}
